<?php

namespace Drupal\agoralocation;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\agoralocation\Entity\LocationInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\node\NodeInterface;

/**
 * The default location renderer implementation.
 */
class LocationRenderer implements LocationRendererInterface {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a LocationRenderer object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager) {
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function renderLocationFieldValuesMultiple(array $entities, $view_mode = 'full') {
    $result = [];
    if (!empty($entities)) {
      foreach ($entities as $entity) {
        $result[] = $this->renderLocationFieldValues($entity, $view_mode);
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function renderLocationFieldValues(LocationInterface $entity, $view_mode = 'full') {
    $fields_array = [];
    if (!empty($entity)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      /** @var \Drupal\agoralocation\Entity\LocationInterface $entity */
      $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);
      $render_display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
      $components = $render_display->getComponents();
      $fields = $entity->getFields();
      foreach ($fields as $field) {
        if (!empty($field) && !$field->isEmpty() && !in_array($field->getName(), $this->getLocationFieldsToSkip())) {
          $field_name = $field->getName();

          // For any other view mode than 'full', check the view display.
          if ($view_mode !== 'full' && !in_array($field_name, array_keys($components))) {
            continue;
          }

          $key = $this->normalizeFieldname($field_name);
          if ($field instanceof FileFieldItemList) {
            $fields_array[$key] = $field->view($view_mode);
          }
          else {
            if ($field_name == 'address') {
              /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address_field */
              $address_field = $field->first();
              $address_values = $this->extractAddressFieldValues($address_field);
              $fields_array += $address_values;
            }
            else {
              if (!empty($field->value)) {
                $values = $field->getValue();
                if (count($values) == 1) {
                  $fields_array[$key] = end($values)['value'];
                }
                else {
                  foreach ($values as $value) {
                    $fields_array[$key][] = $value['value'];
                  }
                }
              }
            }
          }
        }
      }
    }
    return $fields_array;
  }

  /**
   * {@inheritdoc}
   */
  public function renderLocationFieldValuesOfNode(NodeInterface $node, $view_mode = 'full') {
    $result = [];
    if (!empty($node) && $node->hasField('locations') && !$node->get('locations')->isEmpty()) {
      foreach ($node->locations as $location_reference) {
        /** @var \Drupal\agoralocation\Entity\LocationInterface $location */
        $location = $location_reference->entity;
        $result[] = $this->renderLocationFieldValues($location, $view_mode);
      }
    }
    return $result;
  }

  /**
   * Normalizes the given field name by dropping name prefixes.
   *
   * @param string $fieldname
   *   The field name to normalize.
   *
   * @return string
   *   The normalized field name. Common Drupal field name prefixes will be
   *   removed from the field name.
   */
  protected function normalizeFieldname($fieldname) {
    $result = $fieldname;
    if (!empty($fieldname)) {
      $matches = [];
      preg_match("/field_/", $fieldname, $matches);
      if (!empty($matches)) {
        $prefixes = ['field_'];
        return str_replace($prefixes, '', $fieldname);
      }
    }
    return $result;
  }

  /**
   * Extract address field values and returns a flat string array.
   *
   * @param \Drupal\address\Plugin\Field\FieldType\AddressItem $field
   *   The address item.
   *
   * @return string[]
   *   The address field values as flattened string array.
   */
  protected function extractAddressFieldValues(AddressItem $field) {
    $results = [];
    if (!$field->isEmpty()) {
      foreach ($field->getValue() as $key => $value) {
        if (!in_array($key, $this->getAddressFieldsToSkip())) {
          $results[$key] = $value;
        }
      }
    }
    return $results;
  }

  /**
   * Returns a list of location entity fields to exclude from field rendering.
   *
   * @return string[]
   *   A list of location fields to exclude from our custom, flattened field
   *   rendering. These are internal fields like id or uuid, that do not get
   *   exposed in frontend.
   */
  protected function getLocationFieldsToSkip() {
    return [
      'location_id',
      'status',
      'uuid',
      'uid',
      'type',
      'vid',
      'langcode',
      'created',
      'changed',
      'show_in_block',
      'default_langcode',
      '_attributes',
    ];
  }

  /**
   * Returns a list of address fields to exclude from value extraction.
   *
   * @return string[]
   *   A list of address fields to exclude from value extraction. These are
   *   internal fields like langcode, that do not get exposed in frontend.
   */
  protected function getAddressFieldsToSkip() {
    return [
      'langcode',
      'sorting_code',
    ];
  }

}
