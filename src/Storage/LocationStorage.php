<?php

namespace Drupal\agoralocation\Storage;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Default location storage implementation.
 */
class LocationStorage extends SqlContentEntityStorage implements LocationStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadLocationsForBlock() {
    return $this->loadByProperties([
      'show_in_block' => 1,
      'status' => 1,
    ]);
  }

}
