<?php

namespace Drupal\agoralocation\Storage;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

/**
 * Defines the interface for location storage classes.
 */
interface LocationStorageInterface extends SqlEntityStorageInterface {

  /**
   * Loads the location entities for display in the location block.
   *
   * @return \Drupal\agoralocation\Entity\LocationInterface[]
   *   The location entities for display in the location block.
   */
  public function loadLocationsForBlock();

}
