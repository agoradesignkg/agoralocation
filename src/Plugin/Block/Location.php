<?php

namespace Drupal\agoralocation\Plugin\Block;

use Drupal\agoralocation\LocationRendererInterface;
use Drupal\agoralocation\LocationServiceInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides agoralocation Location block for Google Maps integration.
 *
 * @Block(
 *   id = "agoralocation_location",
 *   admin_label = @Translation("Agoralocation Location Block"),
 *   category = @Translation("Agoralocation")
 * )
 */
class Location extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The location renderer.
   *
   * @var \Drupal\agoralocation\LocationRendererInterface
   */
  protected $locationRenderer;

  /**
   * The location service.
   *
   * @var \Drupal\agoralocation\LocationServiceInterface
   */
  protected $locationService;

  /**
   * Constructs a Location object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\agoralocation\LocationRendererInterface $location_renderer
   *   The location renderer.
   * @param \Drupal\agoralocation\LocationServiceInterface $location_service
   *   The location service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityDisplayRepositoryInterface $entity_display_repository, LocationRendererInterface $location_renderer, LocationServiceInterface $location_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityDisplayRepository = $entity_display_repository;
    $this->locationRenderer = $location_renderer;
    $this->locationService = $location_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository'),
      $container->get('agoralocation.renderer'),
      $container->get('agoralocation.location_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $locations = $this->locationService->loadLocationsForBlock();
    $items = $this->locationRenderer->renderLocationFieldValuesMultiple($locations, $this->configuration['view_mode']);

    $output = [];
    if (!empty($items)) {
      $output['#theme'] = 'agoralocation_location';
      $output['#items'] = $items;
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('The view mode that will be used for rendering the locations in the block.'),
      '#default_value' => $this->configuration['view_mode'],
      '#options' => $this->getAvailableViewModes(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['languages']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'node_list:contact',
      'location_list',
    ]);
  }

  /**
   * Gets available view modes of location entities for block form config.
   */
  protected function getAvailableViewModes() {
    $options = [
      // Always add the 'default' view mode.
      'default' => 'Default',
    ];
    $form_modes = $this->entityDisplayRepository->getViewModes('location');
    foreach ($form_modes as $id => $info) {
      $options[$id] = $info['label'];
    }
    return $options;
  }

}
