<?php

namespace Drupal\agoralocation\Plugin\Block;

use Drupal\agoralocation\LocationServiceInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Google Maps integration via Embed API.
 *
 * @Block(
 *   id = "agoralocation_embedded_gmap",
 *   admin_label = @Translation("Agoralocation embedded Google Maps"),
 *   category = @Translation("Agoralocation")
 * )
 */
class EmbeddedGmap extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module's configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The location service.
   *
   * @var \Drupal\agoralocation\LocationServiceInterface
   */
  protected $locationService;

  /**
   * Constructs a EmbeddedGmap object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\agoralocation\LocationServiceInterface $location_service
   *   The location service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LocationServiceInterface $location_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = $config_factory->get('agoralocation.settings');
    $this->locationService = $location_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('agoralocation.location_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'maptype' => 'roadmap',
      'zoom' => 12,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['maptype'] = [
      '#type' => 'select',
      '#title' => $this->t('Map type'),
      '#description' => $this->t('Defines the type of map tiles to load.'),
      '#default_value' => $this->configuration['maptype'],
      '#options' => [
        'roadmap' => $this->t('Road map'),
        'satellite' => $this->t('Satellite'),
      ],
    ];

    $form['zoom'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom level'),
      '#default_value' => $this->configuration['zoom'],
      '#step' => 1,
      '#min' => 0,
      '#max' => 21,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['maptype'] = $form_state->getValue('maptype');
    $this->configuration['zoom'] = $form_state->getValue('zoom');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $gmap_api_key = $this->config->get('gmap_api_key');
    if (empty($gmap_api_key)) {
      return [];
    }

    $location = $this->locationService->loadMainLocation();
    if (empty($location)) {
      return [];
    }

    $address_field_name = 'address';
    if (!$location->hasField($address_field_name) || $location->get($address_field_name)->isEmpty()) {
      return [];
    }

    $address_parts = [];
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $location->get($address_field_name)->first();
    if ($postal_code = $address->getPostalCode()) {
      $address_parts[] = $postal_code;
    }
    if ($city = $address->getLocality()) {
      $address_parts[] = $city;
    }
    if ($street = $address->getAddressLine1()) {
      $address_parts[] = $street;
    }
    $full_address = implode(' ', $address_parts);

    // https://www.google.com/maps/embed/v1/MODE?key=YOUR_API_KEY&parameters
    $url_options = [
      'query' => [
        'key' => $gmap_api_key,
        'q' => $full_address,
        'region' => strtolower($address->getCountryCode()),
        'maptype' => $this->configuration['maptype'],
        'zoom' => $this->configuration['zoom'],
      ],
    ];
    $maps_url = Url::fromUri('https://www.google.com/maps/embed/v1/place', $url_options);
    $output['#theme'] = 'embedded_gmap';
    $output['#url'] = $maps_url->toString();
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = ['config:agoralocation.settings'];
    $location = $this->locationService->loadMainLocation();
    if (!empty($location)) {
      $tags[] = 'location:' . $location->id();
    }
    return Cache::mergeTags(parent::getCacheTags(), $tags);
  }

}
