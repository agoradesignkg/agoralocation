<?php

namespace Drupal\agoralocation\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides agoralocation MapCanvas block for Google Maps integration.
 *
 * @Block(
 *   id = "agoralocation_mapcanvas",
 *   admin_label = @Translation("Agoralocation MapCanvas Block"),
 *   category = @Translation("Agoralocation")
 * )
 */
class MapCanvas extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return ['#markup' => '<div id="map-canvas"></div>'];
  }

}
