<?php

namespace Drupal\agoralocation;

/**
 * Defines an interface for location service.
 */
interface LocationServiceInterface {

  /**
   * Loads the location entities for display in the location block.
   *
   * @return \Drupal\agoralocation\Entity\LocationInterface[]
   *   The location entities for display in the location block.
   */
  public function loadLocationsForBlock();

  /**
   * Loads the main location entity of the page.
   *
   * This is currently determined by the fact, that it must be visible for
   * display in the location block. Currently, no other criteria are added. So,
   * it is in other words, the first item in the result of calling
   * $this->loadLocationsForBlock().
   *
   * @return \Drupal\agoralocation\Entity\LocationInterface|null
   *   The main location of the page, or null, if none was found.
   */
  public function loadMainLocation();

  /**
   * Loads the active contact node.
   *
   * It is assumed that there is only one contact node per site anyway. This
   * function always returns a single node object (or NULL if not found).
   *
   * @return \Drupal\node\NodeInterface|null
   *   The active contact node, or NULL, if not found.
   */
  public function loadContactNode();

}
