<?php

namespace Drupal\agoralocation\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the location add/edit form.
 */
class LocationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\agoralocation\Entity\LocationInterface $location */
    $location = $this->getEntity();
    $location->save();
    $this->messenger()->addMessage($this->t('The location %label has been successfully saved.', ['%label' => $location->label()]));
  }

}
