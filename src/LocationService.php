<?php

namespace Drupal\agoralocation;

use Drupal\agoralocation\Event\FilterLocationsEvent;
use Drupal\agoralocation\Event\LocationEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Default location service implementation.
 */
class LocationService implements LocationServiceInterface {

  /**
   * The location storage.
   *
   * @var \Drupal\agoralocation\Storage\LocationStorageInterface
   */
  protected $locationStorage;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a LocationService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->locationStorage = $entity_type_manager->getStorage('location');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function loadLocationsForBlock() {
    $locations = $this->locationStorage->loadLocationsForBlock();
    $event = new FilterLocationsEvent($locations);
    $this->eventDispatcher->dispatch($event, LocationEvents::FILTER_LOCATIONS);
    return $event->getLocations();
  }

  /**
   * {@inheritdoc}
   */
  public function loadMainLocation() {
    $candidates = $this->loadLocationsForBlock();
    return !empty($candidates) ? reset($candidates) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadContactNode() {
    $nids = $this->nodeStorage->getQuery()
      ->condition('type', 'contact')
      ->condition('status', TRUE)
      ->accessCheck()
      ->execute();

    $nid = !empty($nids) ? reset($nids) : NULL;
    return $nid ? $this->nodeStorage->load($nid) : NULL;
  }

}
