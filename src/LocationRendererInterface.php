<?php

namespace Drupal\agoralocation;

use Drupal\agoralocation\Entity\LocationInterface;
use Drupal\node\NodeInterface;

/**
 * Defines an interface for location renderers.
 */
interface LocationRendererInterface {

  /**
   * Creates a render array of the given location entities.
   *
   * Creates a render array of the given location entities, containing a
   * flattened field render array, containing the raw field values by calling
   * renderLocationFields() for each of them.
   *
   * See renderLocationFields() for more information.
   *
   * @param \Drupal\agoralocation\Entity\LocationInterface[] $entities
   *   The location entities to render.
   * @param string $view_mode
   *   The view mode that should be used for rendering the field values.
   *   Currently this is only affecting file fields.
   *
   * @return array
   *   A render array containing the given locations in first hierarchy level,
   *   and their renderable fields as second level.
   */
  public function renderLocationFieldValuesMultiple(array $entities, $view_mode = 'full');

  /**
   * Creates a flattened render array of the given location entity.
   *
   * Creates a flattened render array of the given location entity, containing
   * the raw field values. The address field "address" is treated specially, as
   * their values are getting flattened directly into the resulting array. File
   * fields will be rendered according to their view mode.
   *
   * @todo This should be refactored definitely! The rendering should always
   *  respect the display settings. The only thing that really makes sense is
   *  the special rendering of the address field. This however could be solved
   *  either by a custom field formatter, or - if we need the info inside the
   *  location's template - added in an view alteration hook instead.
   *  Even if we want to keep this function, we still could and should just
   *  render the location entity and then add our own stuff.
   *
   * @param \Drupal\agoralocation\Entity\LocationInterface $entity
   *   The location entities to render.
   * @param string $view_mode
   *   The view mode that should be used for rendering the field values.
   *   Currently this is only affecting file fields.
   *
   * @return array
   *   A render array containing the a render array containing all the field
   *   values of the given location entity.
   */
  public function renderLocationFieldValues(LocationInterface $entity, $view_mode = 'full');

  /**
   * Creates a flattened render array of the given node's referenced locations.
   *
   * Creates a flattened render array of the given node's referenced location
   * entities, containing the raw field values. This is basically a wrapper
   * around renderLocationFieldValues(), so please see
   * renderLocationFieldValues() for further information.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node containing the location entities to render.
   * @param string $view_mode
   *   The view mode that should be used for rendering the field values.
   *   Currently this is only affecting file fields.
   *
   * @return array
   *   A render array containing the a render array containing all the field
   *   values of the given node's referenced location entities.
   */
  public function renderLocationFieldValuesOfNode(NodeInterface $node, $view_mode = 'full');

}
