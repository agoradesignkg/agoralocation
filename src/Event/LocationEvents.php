<?php

namespace Drupal\agoralocation\Event;

/**
 * Defines events for the agoralocation module.
 */
final class LocationEvents {

  /**
   * Name of the event fired when filtering available locations.
   *
   * @Event
   *
   * @see \Drupal\agoralocation\Event\FilterLocationsEvent
   */
  const FILTER_LOCATIONS = 'agoralocation.filter_locations';

}
