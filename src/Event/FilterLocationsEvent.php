<?php

namespace Drupal\agoralocation\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the event for filtering the available locations.
 */
class FilterLocationsEvent extends Event {

  /**
   * The available locations.
   *
   * @var \Drupal\agoralocation\Entity\LocationInterface[]
   */
  protected $locations;

  /**
   * Constructs a new FilterLocationsEvent object.
   *
   * @param \Drupal\agoralocation\Entity\LocationInterface[] $locations
   *   The locations.
   */
  public function __construct(array $locations) {
    $this->locations = $locations;
  }

  /**
   * Gets the locations.
   *
   * @return \Drupal\agoralocation\Entity\LocationInterface[]
   *   The locations.
   */
  public function getLocations(): array {
    return $this->locations;
  }

  /**
   * Sets the locations.
   *
   * @param \Drupal\agoralocation\Entity\LocationInterface[] $locations
   *   The locations.
   *
   * @return $this
   */
  public function setLocations(array $locations) {
    $this->locations = $locations;
    return $this;
  }

}
