<?php

namespace Drupal\agoralocation\Entity;

use Drupal\address\AddressInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for location entities.
 */
interface LocationInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the location title.
   *
   * @return string
   *   The location title.
   */
  public function getTitle();

  /**
   * Sets the location title.
   *
   * @param string $title
   *   The location title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Get the phone number.
   *
   * @return string
   *   The phone number.
   */
  public function getPhone();

  /**
   * Set the phone number.
   *
   * @param string $phone
   *   The phone number.
   *
   * @return $this
   */
  public function setPhone($phone);

  /**
   * Get the fax number.
   *
   * @return string
   *   The fax number.
   */
  public function getFax();

  /**
   * Set the fax number.
   *
   * @param string $fax
   *   The fax number.
   *
   * @return $this
   */
  public function setFax($fax);

  /**
   * Get the email address.
   *
   * @return string
   *   The email address.
   */
  public function getEmail();

  /**
   * Set the email address.
   *
   * @param string $mail
   *   The email address.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Get the website url.
   *
   * @return \Drupal\Core\Url|null
   *   The website url (as Url object).
   */
  public function getWebsite();

  /**
   * Set the website url.
   *
   * @param string $website
   *   The website url.
   *
   * @return $this
   */
  public function setWebsite($website);

  /**
   * Gets the address.
   *
   * @return \Drupal\address\AddressInterface|null
   *   The address item.
   */
  public function getAddress();

  /**
   * Helper function, returns a geocoding suitable string of the address.
   *
   * @return string
   *   A geocoding suitable string representation of the address.
   */
  public function getAddressString();

  /**
   * Sets the address.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address item.
   *
   * @return $this
   */
  public function setAddress(AddressInterface $address);

  /**
   * Returns whether or not an address is set.
   *
   * @return bool
   *   TRUE, if the partner has an address set, FALSE otherwise.
   */
  public function hasAddress();

  /**
   * Returns whether the location should be shown in contact block.
   *
   * @return bool
   *   TRUE, if the location should be shown in contact block, FALSE otherwise.
   */
  public function isShowInBlock();

  /**
   * Sets whether the location should be shown in contact block.
   *
   * @param bool $show
   *   TRUE, if the location should be shown in contact block, FALSE otherwise.
   *
   * @return $this
   */
  public function setShowInBlock($show);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

}
