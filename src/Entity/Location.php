<?php

namespace Drupal\agoralocation\Entity;

use Drupal\address\AddressInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\link\LinkItemInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the location entity class.
 *
 * @ContentEntityType(
 *   id = "location",
 *   label = @Translation("Location"),
 *   label_singular = @Translation("location"),
 *   label_plural = @Translation("locations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count location",
 *     plural = "@count locations",
 *   ),
 *   bundle_label = @Translation("Location type"),
 *   handlers = {
 *     "storage" = "Drupal\agoralocation\Storage\LocationStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "default" = "Drupal\agoralocation\Form\LocationForm",
 *       "add" = "Drupal\agoralocation\Form\LocationForm",
 *       "edit" = "Drupal\agoralocation\Form\LocationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer agoralocation",
 *   permission_granularity = "entity_type",
 *   base_table = "location",
 *   data_table = "location_data",
 *   translatable = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   entity_keys = {
 *     "id" = "location_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-page" = "/location/add",
 *     "add-form" = "/location/add/{location_type}",
 *     "edit-form" = "/location/{location}/edit",
 *     "delete-form" = "/location/{location}/delete",
 *     "delete-multiple-form" = "/admin/content/locations/delete",
 *     "collection" = "/admin/content/locations",
 *   },
 *   bundle_entity_type = "location_type",
 *   field_ui_base_route = "entity.location_type.edit_form",
 * )
 */
class Location extends ContentEntityBase implements LocationInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhone() {
    return $this->get('phone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhone($phone) {
    $this->get('phone')->value = $phone;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFax() {
    return $this->get('fax')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFax($fax) {
    $this->get('fax')->value = $fax;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    $this->get('mail')->value = $mail;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebsite() {
    $field_item = $this->get('website');
    return !$field_item->isEmpty() ? $field_item->first()->getUrl() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setWebsite($website) {
    $this->get('website')->uri = $website;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    $address_field = $this->get('address');
    return $address_field->isEmpty() ? NULL : $address_field->first();
  }

  /**
   * {@inheritdoc}
   */
  public function getAddressString() {
    if (!$this->hasAddress()) {
      return '';
    }
    /** @var \Drupal\address\AddressInterface $address */
    $address = $this->getAddress();
    $address_values = [];
    if (!empty($address->getAddressLine1())) {
      $address_values[] = $address->getAddressLine1();
    }
    if (!empty($address->getAddressLine2())) {
      $address_values[] = $address->getAddressLine2();
    }
    if (!empty($address->getPostalCode())) {
      $address_values[] = $address->getPostalCode();
    }
    if (!empty($address->getLocality())) {
      $address_values[] = $address->getLocality();
    }
    if (!empty($address->getCountryCode())) {
      $address_values[] = $address->getCountryCode();
    }
    return implode(',', $address_values);
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress(AddressInterface $address) {
    $this->set('address', $address);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAddress() {
    $address = $this->getAddress();
    return !empty($address) && !$address->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function isShowInBlock() {
    return (bool) $this->get('show_in_block')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShowInBlock($show) {
    $this->set('show_in_block', $show);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['status']
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 90,
      ]);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setDescription(new TranslatableMarkup('The location title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Phone'))
      ->setSetting('default_value', '')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 3,
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fax'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Fax'))
      ->setSetting('default_value', '')
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 4,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 4,
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setSetting('default_value', '')
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'weight' => 5,
        'label' => 'inline',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['website'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Website'))
      ->setSetting('default_value', '')
      ->setSetting('link_type', LinkItemInterface::LINK_EXTERNAL)
      ->setSetting('title', DRUPAL_DISABLED)
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'type' => 'link',
        'weight' => 6,
        'label' => 'inline',
        'settings' => [
          'trim_length' => NULL,
          'url_only' => TRUE,
          'target' => '_blank',
          'url_plain' => FALSE,
          'rel' => '0',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // @todo define a setting to narrow down 'available_countries' setting.
    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Address'))
      ->setSetting('field_overrides', static::getAddressFieldOverrides())
      ->setSetting('langcode_override', '')
      ->setDefaultValue(['country_code' => 'AT'])
      ->setDisplayOptions('form', [
        'type' => 'address_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'type' => 'address_default',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['show_in_block'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Show in block'))
      ->setDescription(t('Whether the location should be included in contact block.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time when the location was created.'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time when the location was last edited.'));

    return $fields;
  }

  /**
   * Returns the address field overrides for location address.
   *
   * @return array
   *   An array of used address fields.
   */
  public static function getAddressFieldOverrides() {
    return [
      'administrativeArea' => [
        'override' => 'hidden',
      ],
      'locality' => [
        'override' => 'required',
      ],
      'dependentLocality' => [
        'override' => 'hidden',
      ],
      'postalCode' => [
        'override' => 'required',
      ],
      'sortingCode' => [
        'override' => 'hidden',
      ],
      'addressLine1' => [
        'override' => 'optional',
      ],
      'addressLine2' => [
        'override' => 'optional',
      ],
      'addressLine3' => [
        'override' => 'hidden',
      ],
      'organization' => [
        'override' => 'optional',
      ],
      'givenName' => [
        'override' => 'hidden',
      ],
      'additionalName' => [
        'override' => 'hidden',
      ],
      'familyName' => [
        'override' => 'hidden',
      ],
    ];
  }

}
