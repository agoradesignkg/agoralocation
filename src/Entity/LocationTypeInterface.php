<?php

namespace Drupal\agoralocation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for location types.
 */
interface LocationTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
