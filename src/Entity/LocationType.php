<?php

namespace Drupal\agoralocation\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the location type entity class.
 *
 * @ConfigEntityType(
 *   id = "location_type",
 *   label = @Translation("Location type"),
 *   label_singular = @Translation("location type"),
 *   label_plural = @Translation("location types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count location type",
 *     plural = "@count location types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\agoralocation\Form\LocationTypeForm",
 *       "edit" = "Drupal\agoralocation\Form\LocationTypeForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\agoralocation\LocationTypeListBuilder",
 *   },
 *   config_prefix = "location_type",
 *   admin_permission = "administer agoralocation",
 *   bundle_of = "location",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/location-types/add",
 *     "edit-form" = "/admin/structure/location-types/{location_type}/edit",
 *     "collection" = "/admin/structure/location-types"
 *   }
 * )
 */
class LocationType extends ConfigEntityBundleBase implements LocationTypeInterface {

  /**
   * The location type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The location type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The location type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
