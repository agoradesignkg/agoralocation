/**
 * @file
 * JavaScript initializing gmap3 library.
 */

(function ($) {

Drupal.behaviors.initGmap3 = {
  attach: function (context, settings) {
    var canvasSelector = '#map-canvas';

    let mapCanvasItems = once('gmap3', $(context).find(canvasSelector));
    $(mapCanvasItems).each(function () {
      var $mapCanvas = $(this);

      var mapOptions = {
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        scrollwheel: false,
        streetViewControl: true,
        draggable: true
      };

      var customStyles = settings['gmap3']['styles'];
      if (customStyles) {
        mapOptions.styles = customStyles;
      }

      var geocodingOptions = settings['gmap3']['geocoding'];

      var autofit = settings['gmap3']['autofit'] ? true : false;

      var markerSelector = settings['gmap3']['markerSelector'];
      if (!markerSelector) {
        markerSelector = '.vcard';
      }

      var routeElemSelector = settings['gmap3']['routeElemSelector'];
      if (!routeElemSelector) {
        routeElemSelector = '#route';
      }

      var directionsPanelSelector = settings['gmap3']['directionsPanelSelector'];
      if (!directionsPanelSelector) {
        directionsPanelSelector = '#directions-panel';
      }

      var routeCalcSelector = settings['gmap3']['routeCalcSelector'];
      if (!routeCalcSelector) {
        routeCalcSelector = '#route-calc';
      }

      var routeResetSelector = settings['gmap3']['routeResetSelector'];
      if (!routeResetSelector) {
        routeResetSelector = '#route-reset';
      }

      var routeOriginSelector = settings['gmap3']['routeOriginSelector'];
      if (!routeOriginSelector) {
        routeOriginSelector = '#route-origin';
      }

      var customIcon = settings['gmap3']['icon'];
      if (customIcon && customIcon.path && customIcon.path == 'fontawesome.markers.MAP_MARKER') {
        // TODO This is a workaround. We must find a way to pass a JS variable instance per Drupal settings.
        customIcon.path = fontawesome.markers.MAP_MARKER;
      }

      var $vcards = $(markerSelector);
      if ($mapCanvas.length && $vcards.length) {
        var markers = [];
        var count = 1;
        var defaultZoom = 13;
        if ($mapCanvas.data('zoom')) {
          defaultZoom = $mapCanvas.data('zoom');
        }

        $vcards.each(function (idx, vcard) {
          var $vcard = $(vcard);
          var marker = createMapMarker($vcard, "marker-" + count);
          if (marker) {
            if (customIcon) {
              marker.options = {
                icon: customIcon
              };
            }
            markers.push(marker);
            count++;
          }
        });

        if (markers.length) {
          markers[0].tag = 'defaultMarker';

          initGmap($mapCanvas, markers, defaultZoom, mapOptions, autofit);

          $(routeElemSelector).show();
          var $directionsPanel = $(directionsPanelSelector);
          var $routeCalc = $(routeCalcSelector);
          var $routeReset = $(routeResetSelector);
          var $routeOrigin = $(routeOriginSelector);

          $routeCalc.click(function (e) {
            e.preventDefault();
            var origin = $routeOrigin.val();
            var routeOptions = {
              travelMode: google.maps.DirectionsTravelMode.DRIVING,
              origin: origin,
              destination: markers[0]
            };

            if (geocodingOptions && geocodingOptions.region) {
              routeOptions.region = geocodingOptions.region;
            }

            $directionsPanel.empty();

            $mapCanvas.gmap3({
              clear: {
                name: ["directionsrenderer", "marker"]
              }
            });

            $mapCanvas.gmap3({
              getroute:{
                options: routeOptions,
                callback: function (results) {
                  if (!results) {
                    return;
                  }
                  $directionsPanel.show();
                  $routeReset.show();
                  $directionsPanel.trigger('routeReady.gmap');

                  $(this).gmap3({
                    directionsrenderer:{
                      container: $directionsPanel,
                      options:{
                        directions:results
                      }
                    }
                  });
                }
              }
            });
          });

          $routeReset.click(function (e) {
            e.preventDefault();

            $mapCanvas.gmap3({
              clear: {
                name: ["directionsrenderer"]
              }
            });

            initGmap($mapCanvas, markers, defaultZoom, mapOptions, autofit);

            $directionsPanel.empty().hide();

            $routeReset.hide();
          });

          $routeOrigin.keydown(function (e) {
            if (e.keyCode == 13) {
              $routeCalc.click();
            }
          });
        }
      }
    });
  }
};

function initGmap($gmapContainer, markers, zoom, mapOptions, autofit) {
  var options = mapOptions;
  if (markers.length == 1) {
    // Center map for single marker - multiple markers will be positioned after initalization.
    options.center = markers[0];
  }
  options.zoom = zoom;

  var mapConfiguration = { options: options };
  var isGeocoded = markers[0].latLng ? true : false;
  if (!isGeocoded) {
    mapConfiguration.address = markers[0].address;
  }

  var $map = $gmapContainer.gmap3({
    map: mapConfiguration,
    marker:{
      values: markers,
      events:{
        mouseover: function (marker, event, context) {
          var map = $(this).gmap3("get"),
            infowindow = $(this).gmap3({get:{name:"infowindow"}});
          if (infowindow) {
            infowindow.open(map, marker);
            infowindow.setContent(context.data);
          }
          else {
            $(this).gmap3({
              infowindow:{
                anchor:marker,
                options:{content: context.data}
              }
            });
          }
        },
        mouseout: function () {
          var infowindow = $(this).gmap3({get:{name:"infowindow"}});
          if (infowindow) {
            infowindow.close();
          }
        }
      }
    }
  });
  if (autofit) {
    $map.gmap3("autofit");
  }
  var map = $map.gmap3("get");

}

function createMapMarker($vcard, markerId) {
  var lat = $vcard.data('lat');
  var lng = $vcard.data('lng');
  var name = $vcard.data('name');
  var organization = $vcard.find('.organization').text();
  if (!organization) {
    organization = $vcard.find('.org').length ? $vcard.find('.org').text() : '';
  }
  organization = organization.trim();
  if (!name) {
    name = organization ? organization : '';
  }

  var recipient = $vcard.find('.recipient').text().trim();
  var address = $vcard.find('.address-line1').text().trim();
  if (!address) {
    address = $vcard.find('.street-address').text().trim();
  }
  var postalCode = $vcard.find('.postal-code').text().trim();
  var city = $vcard.find('.locality').text().trim();
  var fullAddress = '';
  if (address != '' && postalCode != '' && city != '') {
    fullAddress = postalCode + ' ' + city + ', ' + address;
  }

  var marker = null;
  if (lat && lng) {
    marker = {
      latLng: [lat,lng],
      data: name + '<br/>' + fullAddress,
      id: markerId
    };
  }
  else {

    if (recipient && name != recipient) {
      name += '<br/>' + recipient;
    }

    if (fullAddress != '') {
      marker = {
        address: fullAddress,
        data: name + '<br/>' + fullAddress,
        id: markerId
      };
    }
  }
  return marker;
}

})(jQuery);
